// simple_int_set.h

#ifndef _SIMPLE_INT_SET_H_
#define _SIMPLE_INT_SET_H_

#include <vector>

class SimpleIntSet {
 public:
  SimpleIntSet();
  SimpleIntSet(const SimpleIntSet& int_set);
  ~SimpleIntSet();

  SimpleIntSet Intersect(const SimpleIntSet& int_set) const;
  SimpleIntSet Union(const SimpleIntSet& int_set) const;
  SimpleIntSet Difference(const SimpleIntSet& int_set) const;

  void Set(const int* values, size_t size);

  const int* values() const { return values_.data(); }
  size_t size() const { return values_.size(); }

 private:
  // Feel free to add private utility functions.

  std::vector<int> values_;
};

#endif  // _SIMPLE_INT_SET_H_
