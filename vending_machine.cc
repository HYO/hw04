#include <iostream>
#include <string>
#include <vector>
#include "vending_machine.h"
#include <map>

using namespace std;

VendingMachine::VendingMachine() {
}

void VendingMachine::AddBeverage(const std::string& name, int unit_price, int stock) {
	beverages_[name].unit_price=unit_price;
	if(beverages_[name].stock>0) 
		beverages_[name].stock=beverages_[name].stock+stock;
	else beverages_[name].stock=stock;
	beverages_[name].name=name;
}

int VendingMachine::Buy(const std::map<string, int>& items, int money) {
	map<string, int>::const_iterator it;
	for( it = items.begin(); it != items.end(); ++it ) {
		if(beverages_[it->first].name!=it->first) return -3;
		if(beverages_[it->first].stock-it->second<0) return -2;
	}
	for( it = items.begin(); it != items.end(); ++it ) {
		beverages_[it->first].stock-=it->second;
		money-=(beverages_[it->first].unit_price)*it->second;
	}
	if(money<0) {
		for( it = items.begin(); it != items.end(); ++it ) {
			beverages_[it->first].stock+=it->second;
		}
		return -1;
	}
	return money;


}

