// blackjack.cc

#include <iostream>
#include <string>
#include <vector>
using namespace std;

// Implement this function.
int BlackJack(const vector<string>& cards);

int main() {
  while (true) {
    vector<string> cards;
    int num_cards;
    cin >> num_cards;
    if ((cin.rdstate() & istream::failbit) != 0) break;
    cards.resize(num_cards);
    for (int i = 0; i < num_cards; ++i) cin >> cards[i];

    int ret = BlackJack(cards);
    if (ret < 0) break;
    else if (ret == 0) cout << "Exceed" << endl;
    else if (ret == 21) cout << "BlackJack" << endl;
    else cout << ret << endl;
  }
  return 0;
}
int BlackJack(const vector<string>& cards) {
	int sum=0;
	int A_num=0;
	for(int i=0; i<cards.size(); i++) {
		if(cards[i] == "10" ||cards[i]=="K"||cards[i]=="J"||cards[i]=="Q") sum+=10;
		else if (cards[i]=="A") A_num+=1;
		else sum=sum + cards[i][0]-'0';
	}
	while(A_num!=0) {
		if(sum+12-A_num<=21) {
			A_num-=1;
			sum+=11;
		}
		else {
			sum+=1;
			A_num-=1;
		}
	}
	if(sum>21) return 0;
	return sum;
}