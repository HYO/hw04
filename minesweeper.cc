#include <iostream>
#include <string>
#include <vector>
#include "minesweeper.h"

using namespace std;

Minesweeper::Minesweeper() {
	width_=0;
	height_=0;
	map_.resize(height_);
}
Minesweeper::~Minesweeper() {
	width_=0;
	height_=0;
}

bool Minesweeper::SetMap(size_t w, size_t h, const std::vector<std::string>& map) {
	width_=w;
	height_=h;
	map_.resize(h);
	for(int i=0;i<h;i++) 
		map_[i]=map[i];
	return true;
}

bool Minesweeper::ToggleMine(int x, int y) {
	if(x>width_||y>height_) return false;
	if(map_[y][x]=='*')map_[y][x]='.';
	else map_[y][x]='*';
	return true;
}

size_t Minesweeper::width() const {
	return width_;
}

size_t Minesweeper::height() const {
	return height_;
}
char Minesweeper::get(int x, int y) const {
	int i=0,m=-1,o=-1,n=2,p=2;
	if(map_[y][x]=='*') return '*';
	if(x==0) {m++; cout<<endl;}
	if(y==0) o++;
	if(x==(width_-1)) n=1;
	if(y==(height_-1)) p=1;
	for(int k=m;k<n; k++) 
		for(int l=o; l<p; l++) 
			if(map_[y+l][x+k]=='*') i++;
	char result='0'+i;
	return result;
}
