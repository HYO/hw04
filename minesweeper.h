#ifndef _MINESWEEPER_H_
#define _MINESWEEPER_H_

#include <string>
#include <vector>

class Minesweeper {
 public:
  Minesweeper();
  ~Minesweeper();

  bool SetMap(size_t w, size_t h, const std::vector<std::string>& map);
  bool ToggleMine(int x, int y);

  size_t width() const;
  size_t height() const;
  char get(int x, int y) const;

 private:
  size_t width_,height_;
  std::vector<std::string> map_;


};

#endif  // _MINESWEEPER_H_
