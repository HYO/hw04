// minesweeper_main.cpp

#include <iostream>
#include <string>
#include <vector>
#include "minesweeper.h"

using namespace std;

void PrintMinesweeperMap(const Minesweeper& ms) {
  const int w = ms.width(), h = ms.height();
  for (int y = 0; y < h; ++y) {
    for (int x = 0; x < w; ++x) cout << ms.get(x, y);
    cout << endl;
  }
}

int main() {
  Minesweeper ms;
  while (true) {
    string cmd;
    cin >> cmd;
    if (cmd == ":set") {
      size_t w, h;
      cin >> w >> h;
      if ((cin.rdstate() & istream::failbit) != 0) break;
      vector<string> map;
      map.resize(h);
      for (int i = 0; i < h; ++i) cin >> map[i];
      if (ms.SetMap(w, h, map) == false) {
        cout << "failed to set map." << endl;
      } else {
        PrintMinesweeperMap(ms);
      }
    } else if (cmd == ":toggle") {
      int x, y;
      cin >> x >> y;
      if ((cin.rdstate() & istream::failbit) != 0) break;
      if (ms.ToggleMine(x, y) == false) {
        cout << "failed to toggle a mine." << endl;
      } else {
        PrintMinesweeperMap(ms);
      }
    } else {
      break;
    }
  }
  return 0;
}

